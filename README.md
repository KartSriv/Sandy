<h1 align="center">
  <br>
   Sandy ⌛
  <br>
</h1>

<h4 align="center">A command-line <a href="https://en.wikipedia.org/wiki/Hourglass" target="_blank">Hourglass</a>.</h4>

  <a href="https://github.com/KartSriv/Sandy">
      <img class="center" src="https://raw.githubusercontent.com/KartSriv/Sandy/master/README.gif"><br><br>
  </a>
    </a>
  <a href="https://saythanks.io/to/KartSriv"><img src="https://img.shields.io/badge/SayThanks.io-%E2%98%BC-1EAEDB.svg"></a><a href="https://app.codacy.com/app/KartSriv/Sandy?utm_source=github.com&utm_medium=referral&utm_content=KartSriv/Sandy&utm_campaign=Badge_Grade_Dashboard"> <img src="https://api.codacy.com/project/badge/Grade/71a9f20ff0094cfbb73ca70ebdea381a"></a>
</p>

# Installation
Just type in this code and let the magic begin. <br>
```wget https://raw.githubusercontent.com/KartSriv/Sandy/master/Install.sh && sudo chmod +x Install.sh && sudo ./Install.sh && rm -r Install.sh ``` <br>
# Usage
```
$ Sandy 10 Seconds # You can use secs or sec too
$ Sandy 10 Minutes # You can use mins or min too
$ Sandy 10 Hours # You can use hrs or hr too
```
# Dependencies

``` ruby-full ``` for ``` gem ``` and ``` lolcat ``` for colours.<br>
**Dependencies are automatically installed by Install.sh**
<br>



[![forthebadge made-with-python](http://ForTheBadge.com/images/badges/made-with-python.svg)](https://www.python.org/)
[![ForTheBadge built-with-science](http://ForTheBadge.com/images/badges/built-with-science.svg)](https://github.com/KartSriv/)
[![ForTheBadge built-with-love](http://ForTheBadge.com/images/badges/built-with-love.svg)](https://github.com/KartSriv/)
