#!/bin/python

import sys # Duration
import time # Sleeping
from os import system, name  # OS Version for Clear()

try:
    Time = int(sys.argv[1])
except:
    print("Please provide the time for the Sandclock to get exhausted.")
    exit()

try:
    Unit = str(sys.argv[2])
except:
    print("Please provide the unit for the time of the Sandclock to get exhausted.")
    exit()

if Unit.lower() in ['hrs', 'hours', 'hour', 'hr']:
    Time = Time * 60 * 60
elif Unit.lower() in ['min', 'minute', 'minutes', 'mins']:
    Time = Time * 60
elif Unit.lower() in ['sec', 'seconds', 'second', 'secs']:
    pass

Division = round(Time / 4)

def Clear(): 
  
    # for windows 
    if name == 'nt': 
        _ = system('cls') 
  
    # for mac and linux(here, os.name is 'posix') 
    else: 
        _ = system('clear') 
  

def Variation1(Instance):
    """
    with open('Sandclock.' + Instance + '.Loading.Variation1.txt') as File:
        for Sandy in File:
            print (Sandy)
            """
    system('cat /bin/Sandclock.' + Instance + '.Loading.Variation1.txt')

def Variation2(Instance):
    """
    with open('Sandclock.' + Instance + '.Loading.Variation2.txt') as File:
        for Sandy in File:
            print (Sandy)
            """
    system('cat /bin/Sandclock.' + Instance + '.Loading.Variation2.txt')

def LoadingVariation(Instance):
    Variation1(Instance)
    time.sleep(.5)
    Clear()
    Variation2(Instance)
    time.sleep(.5)
    Clear()

def FullyUp():
    """
    with open('Sandclock.0.txt') as File:
        for Sandy in File:
            print (Sandy)
        """
    Clear()
    system('cat /bin/Sandclock.0.txt')
    Clear()

def Completed():
    """
    with open('Sandclock.Completed.txt') as File:
        for Sandy in File:
            print (Sandy)
        """
    system('cat /bin/Sandclock.Completed.txt')

def Loading(Division):
    Clear()
    FullyUp()
    # Division 1
    Iteration = 0
    while Iteration != Division:
        LoadingVariation("0")
        Iteration = Iteration + 1
    # Division 2
    Iteration = 0
    while Iteration != Division:
        LoadingVariation("25")
        Iteration = Iteration + 1
    # Division 3
    Iteration = 0
    while Iteration != Division:
        LoadingVariation("50")
        Iteration = Iteration + 1
    # Division 4
    Iteration = 0
    while Iteration != Division:
        LoadingVariation("75")
        Iteration = Iteration + 1
    Completed()


Loading(Division)
